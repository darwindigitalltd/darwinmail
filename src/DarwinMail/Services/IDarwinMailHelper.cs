using DarwinMail.Models;
using DarwinMail.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using Umbraco.Core.Models.PublishedContent;

namespace DarwinMail.Services
{
    public interface IDarwinMailHelper
    {
        IPublishedContent GetMailNodeById(int nodeId);
        bool SendMail(int nodeId, MailAddress from, ICollection<MailAddress> to,
            ICollection<MailAddress> cc = null,
            ICollection<MailAddress> bcc = null, ICollection<EmailTagModel> tags = null,
            ICollection<Attachment> attachments = null);

        MailMessage CreateMailMessage(string template, int nodeId, MailAddress from,
            ICollection<MailAddress> to, string subject = "", ICollection<MailAddress> cc = null,
            ICollection<MailAddress> bcc = null, ICollection<EmailTagModel> tags = null,
            ICollection<Attachment> attachments = null);

        bool ResendEmail(int emailId);
        bool SendTestMail(int nodeId);

        (int, int) GetSentStatisticForMailNode(int nodeId);

        ICollection<EmailDto> GetSentEmails(int nodeId, int pageSize, int page, string email,
            DateTime? from, DateTime? to, string status);

        ICollection<Attachment> GetAttachmentsForEmail(int emailId);
    }
}
