using DarwinMail.Models;
using DarwinMail.Models.Entities;
using DarwinMail.Repositories;

using System;
using System.Collections.Generic;
using System.Net.Mail;

using Umbraco.Core.Logging;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace DarwinMail.Services
{
    public abstract class DarwinMailHelperBase : IDarwinMailHelper
    {
        protected readonly IMailLogRepository _mailLogRepository;
        protected readonly ILogger _logger;
        protected readonly UmbracoHelper _helper;
        protected readonly static string _templateAlias = "template";
        protected readonly static string _subjectAlias = "subject";
        protected readonly static string _to = "to";
        protected readonly static string _from = "from";
        protected readonly static string _cc = "CC";
        protected readonly static string _bcc = "BCC";
        protected readonly static string _customtags = "customTags";
        public DarwinMailHelperBase(IMailLogRepository mailLogRepository,
            ILogger logger, UmbracoHelper helper)
        {
            _mailLogRepository = mailLogRepository;
            _helper = helper;
            _logger = logger;
        }

        public abstract IPublishedContent GetMailNodeById(int nodeId);
        public abstract bool SendMail(int nodeId, MailAddress from, ICollection<MailAddress> to, ICollection<MailAddress> cc = null, ICollection<MailAddress> bcc = null, ICollection<EmailTagModel> tags = null, ICollection<Attachment> attachments = null);
        public abstract MailMessage CreateMailMessage(string template, int nodeId, MailAddress from, ICollection<MailAddress> to, string subject = "", ICollection<MailAddress> cc = null, ICollection<MailAddress> bcc = null, ICollection<EmailTagModel> tags = null, ICollection<Attachment> attachments = null);
        public abstract bool ResendEmail(int emailId);
        public abstract bool SendTestMail(int nodeId);
        public abstract (int, int) GetSentStatisticForMailNode(int nodeId);
        public abstract ICollection<EmailDto> GetSentEmails(int nodeId, int pageSize, int page, string email, DateTime? from, DateTime? to, string status);
        public abstract ICollection<Attachment> GetAttachmentsForEmail(int emailId);
    }
}
