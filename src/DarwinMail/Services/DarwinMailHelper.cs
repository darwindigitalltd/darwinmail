using DarwinMail.Constants;
using DarwinMail.Exceptions;
using DarwinMail.Models;
using DarwinMail.Models.Entities;
using DarwinMail.Repositories;
using DarwinMail.Services;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using Umbraco.Core.Logging;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace DarwinMail
{
    public class DarwinMailHelper : DarwinMailHelperBase
    {
        public DarwinMailHelper(IMailLogRepository mailLogRepository,
            ILogger logger, UmbracoHelper helper) : base(mailLogRepository, logger, helper) { }

        /// <summary>
        /// Gets the given mail node
        /// </summary>
        /// <param name="nodeId">The id of the mail node to retrieve</param>
        /// <returns></returns>
        public override IPublishedContent GetMailNodeById(int nodeId)
        {
            var node = _helper.Content(nodeId);
            if (node == null)
                throw new DarwinMailNodeNotFoundException($"Unable to find node with id: {nodeId}");

            if (!string.Equals(node.ContentType.Alias,
                DarwinMailConstants.DocTypeAlias.MailNodeDocTypeAlias, StringComparison.CurrentCultureIgnoreCase))
                throw new DocTypeAliasException($"Mail node with id {nodeId} was of type {node.ContentType.Alias}. " +
                    $"Must be a {DarwinMailConstants.DocTypeAlias.MailNodeDocTypeAlias} doctype node.");

            return node;
        }

        /// <summary>
        /// Gets the count of sent and failed emails for a mail node
        /// </summary>
        /// <param name="nodeId">The id of the mail node</param>
        /// <returns>Total emails sent</returns>
        public override (int, int) GetSentStatisticForMailNode(int nodeId)
        {
            try
            {
                return _mailLogRepository.GetSentStatisticForMailNode(nodeId);
            }
            catch (Exception e)
            {
                _logger.Error<DarwinMailHelper>($"Unable to get sent mail count for node Id {nodeId}");
                throw;
            }
        }

        /// <summary>
        /// RETURNS EMPTY COLLECTION. User most implement attachement retrieval.
        /// </summary>
        /// <param name="emailId">The id of the email for which to retrieve attachements.</param>
        /// <returns>EMPTY COLLECTION</returns>
        public override ICollection<Attachment> GetAttachmentsForEmail(int emailId)
        {
            return new List<Attachment>();
        }

        /// <summary>
        /// Retrieves and resends the email with the given id.
        /// </summary>
        /// <param name="emailId"></param>
        /// <returns></returns>
        public override bool ResendEmail(int emailId)
        {
            var email = _mailLogRepository.GetEmailById(emailId);
            if (email is null)
            {
                _logger.Warn<DarwinMailHelper>($"Unable to find mail node with id: {email}");
                return false;
            }

            var node = GetMailNodeById(email.MailNodeId);
            var template = node.Value<string>(_templateAlias);
            var subject = node.Value<string>(_subjectAlias);
            var from = new MailAddress(email.From.Email, email.From.Name);
            var to = email.To.Select(x => new MailAddress(x.Email, x.Name));
            var cc = email.CC.Select(x => new MailAddress(x.Email, x.Name));
            var bcc = email.BCC.Select(x => new MailAddress(x.Email, x.Name));
            var attachments = GetAttachmentsForEmail(emailId);
            var message = CreateMailMessage(template, node.Id, from, to.ToList(), subject, cc.ToList(), bcc.ToList(), email.Body, attachments);
            return Send(message);
        }


        /// <summary>
        /// Creates a new mail message.
        /// </summary>
        /// <param name="template">The template from which to construct the body of the email</param>
        /// <param name="emailNodeId">The email node id for the email being sent.</param>
        /// <param name="from">Email from</param>
        /// <param name="to">Email receipients</param>
        /// <param name="subject">Email subject</param>
        /// <param name="cc">CC email receipients</param>
        /// <param name="bcc">BCC email receipients</param>
        /// <param name="tags">The tags to transpose into the email given template</param>
        /// <param name="attachments">Email attachements.</param>
        /// <returns></returns>
        public override MailMessage CreateMailMessage(string template, int emailNodeId, MailAddress from,
            ICollection<MailAddress> to, string subject = "", ICollection<MailAddress> cc = null,
            ICollection<MailAddress> bcc = null, ICollection<EmailTagModel> tags = null,
            ICollection<Attachment> attachments = null)
        {
            MandateHasReceipient(to);
            MandateNotNull(from, nameof(from));

            try
            {
                cc = cc ?? new List<MailAddress>();
                bcc = bcc ?? new List<MailAddress>();
                tags = tags ?? new List<EmailTagModel>();
                attachments = attachments ?? new List<Attachment>();

                var message = new MailMessage();
                message.From = from;
                foreach (var item in to.Where(x => x != null))
                {
                    message.To.Add(new MailAddress(item.Address, item.DisplayName));
                }

                foreach (var item in cc.Where(x => x != null))
                {
                    message.CC.Add(new MailAddress(item.Address, item.DisplayName));
                }

                foreach (var item in bcc.Where(x => x != null))
                {
                    message.Bcc.Add(new MailAddress(item.Address, item.DisplayName));
                }

                message.Subject = subject;
                var body = PopulateTemplateWithTags(template, tags);
                message.Body = body;
                message.IsBodyHtml = !string.IsNullOrWhiteSpace(body);
                foreach (var attachment in attachments?.Where(x => x != null))
                {
                    message.Attachments.Add(attachment);
                }

                var emailDto = new EmailDto();
                emailDto.To.AddRange(to.Select(x => new NameEmailPair(x.DisplayName, x.Address)));
                emailDto.CC.AddRange(cc.Select(x => new NameEmailPair(x.DisplayName, x.Address)));
                emailDto.BCC.AddRange(bcc.Select(x => new NameEmailPair(x.DisplayName, x.Address)));
                emailDto.From = new NameEmailPair(from.DisplayName, from.Address);
                emailDto.Body = message.Body;
                emailDto.Date = DateTime.Now;
                emailDto.MailNodeId = emailNodeId;
                emailDto.Subject = subject;

                return message;
            }
            catch (Exception e)
            {
                var error = new DarwinEMailCreationException($"Error attempting to create MailMessage", e);
                _logger.Error<DarwinMailHelper>(error, error.Message);
                throw error;
            }
        }

        /// <summary>
        /// Gets a collection of previously sent emails for the email node given by emailNodeId
        /// </summary>
        /// <param name="emailNodeId">The email node id</param>
        /// <param name="pageSize">number of emails per page for pagination</param>
        /// <param name="page">The page at which to start pagination</param>
        /// <param name="receipientEmail">The receipient email address on which to filter</param>
        /// <param name="sentFrom">The sent from date on which to filter<</param>
        /// <param name="sentTo">The sent to date on which to filter<</param>
        /// <param name="status">The sent status on which to filter<</param>
        /// <returns></returns>
        public override ICollection<EmailDto> GetSentEmails(int emailNodeId, int pageSize, int page, string receipientEmail, DateTime? sentFrom, DateTime? sentTo, string status)
        {
            return _mailLogRepository.GetSentEmailsByNode(emailNodeId, pageSize, page, receipientEmail, sentFrom, sentTo, status);
        }

        /// <summary>
        /// Creates and sends email with the given params
        /// </summary>
        /// <param name="nodeId">The mail node id for mwhich the email template will be pulled.</param>
        /// <param name=from>The senders email address</param>
        /// <param name=_to>The receipingts</param>
        /// <param name="cc">The cc'd receipients</param>
        /// <param name=_bcc>The bcc'd receipients</param>
        /// <param name="tags">The tags to be inserted into the template</param>
        /// <param name="attachments">Attachement for the email</param>
        /// <returns></returns>
        public override bool SendMail(int nodeId, MailAddress from, ICollection<MailAddress> to,
            ICollection<MailAddress> cc = null, ICollection<MailAddress> bcc = null,
            ICollection<EmailTagModel> tags = null, ICollection<Attachment> attachments = null)
        {
            var node = GetMailNodeById(nodeId);
            var template = node.Value<string>(_templateAlias);
            var subject = node.Value<string>(_subjectAlias);
            var message = CreateMailMessage(template, nodeId, from, to, subject, cc, bcc, tags, attachments);
            bool sent = false;
            try
            {
                sent = SendAndLogToDatabase(message, nodeId);
                LogSentMailMessageToDatabase(nodeId, message, sent);
                return sent;
            }
            catch (Exception e)
            {
                LogSentMailMessageToDatabase(nodeId, message, sent, e);
                throw e;
            }
        }

        /// <summary>
        /// Sends an  email using the values (template, to, from, cc etc) from the given mail node.
        /// </summary>
        /// <param name="nodeId">The id of the mail node to be used.</param>
        /// <returns></returns>
        public override bool SendTestMail(int nodeId)
        {
            try
            {
                var node = GetMailNodeById(nodeId);
                var template = node.Value<string>(_templateAlias);
                var message = CreateMailMessage(node, template);
                var attachment = node.Value("attachment");
                

                if (!string.IsNullOrEmpty(attachment.ToString()))
                {
                    string path = HttpContext.Current.Server.MapPath(attachment.ToString());
                    message.Attachments.Add(new Attachment(path));
                }
                return SendAndLogToDatabase(message, nodeId);
            }
            catch (Exception e)
            {
                _logger.Error<DarwinMailHelper>(e, $"Failure sending test email.");
                return false;
            }
        }

        private void LogSentMailMessageToDatabase(int nodeId, MailMessage message, bool sent, Exception exception = null)
        {
            var dto = new EmailDto();
            dto.To.AddRange(message.To.Select(x => new NameEmailPair(x.DisplayName, x.Address)));
            dto.CC.AddRange(message.CC.Select(x => new NameEmailPair(x.DisplayName, x.Address)));
            dto.BCC.AddRange(message.Bcc.Select(x => new NameEmailPair(x.DisplayName, x.Address)));
            dto.From = new NameEmailPair(message.From.DisplayName, message.From.Address);
            dto.Body = message.Body;
            dto.Date = DateTime.Now;
            dto.MailNodeId = nodeId;
            dto.Subject = message.Subject;
            dto.Sent = sent;
            dto.Exception = exception;
            _mailLogRepository.Create(dto);
        }

        private bool SendAndLogToDatabase(MailMessage message, int nodeId)
        {
            try
            {
                Send(message);
                LogSentMailMessageToDatabase(nodeId, message, true);
                return true;
            }
            catch (SmtpException se)
            {
                LogSentMailMessageToDatabase(nodeId, message, false, se);
                _logger.Error<DarwinMailHelper>(se, $"SMTP exception sending email. Error: {se.Message}");
                throw;
            }
            catch (Exception e)
            {
                LogSentMailMessageToDatabase(nodeId, message, false, e);
                _logger.Error<DarwinMailHelper>(e, $"Error sending email. Error: {e.Message}");
                throw;
            }
        }

        private bool Send(MailMessage message)
        {
            var smtpClient = new SmtpClient();
            smtpClient.Send(message);
            return true;
        }

        private MailMessage CreateMailMessage(IPublishedContent node, string template)
        {
            try
            {
                // Get sending values or fallback to parent node vals.
                var to = node.Value<List<NameEmailPair>>(_to)?.Select(x => new MailAddress(x.Email, x.Name))?.ToList();
                to = to == null || !to.Any() ? node.Parent.Value<List<NameEmailPair>>(_to)
                    ?.Select(x => new MailAddress(x.Email, x.Name))?.ToList() : to;

                var cc = node.Value<List<NameEmailPair>>(_cc)?.Select(x => new MailAddress(x.Email, x.Name))?.ToList();
                cc = cc == null || !cc.Any() ? node.Parent.Value<List<NameEmailPair>>("cc")
                    ?.Select(x => new MailAddress(x.Email, x.Name))?.ToList() : cc;

                var bcc = node.Value<List<NameEmailPair>>(_bcc)?.Select(x => new MailAddress(x.Email, x.Name))?.ToList();
                bcc = bcc == null || !bcc.Any() ? node.Parent.Value<List<NameEmailPair>>(_bcc)
                    ?.Select(x => new MailAddress(x.Email, x.Name))?.ToList() : bcc;

                var subject = node.Value<string>(_subjectAlias);
                subject = string.IsNullOrWhiteSpace(subject) ? node.Parent.Value<string>(_subjectAlias) : subject;
                var fromVals = node.Value<NameEmailPair>(_from);
                fromVals = fromVals != null ? fromVals : node.Parent.Value<NameEmailPair>(_from);

                var tags = node.Value<List<EmailTagModel>>(_customtags);
                tags = tags == null || !tags.Any() ? node.Parent.Value<List<EmailTagModel>>(_customtags)
                    ?.Select(x => new EmailTagModel(x.Tag, x.Value))?.ToList() : tags;


                var attachments = GetAttachmentsForEmail(node.Id);

                if (fromVals == null)
                {
                    throw new Exception("No 'from' email addres ahs been provided either " +
                        "on the email node, or in the global settings.");
                }

                var from = new MailAddress(fromVals.Email, fromVals.Name);

                var message = CreateMailMessage(template, node.Id, from, to, subject, cc, bcc, tags, attachments);

                return message;
            }
            catch (Exception e)
            {
                var error = new DarwinEMailCreationException($"Error attempting to create MailMessage", e);
                _logger.Error<DarwinMailHelper>(error, error.Message);
                throw;
            }
        }

        private MailMessage CreateMailMessage(string template, int nodeId, MailAddress from,
            ICollection<MailAddress> to, string subject = "", ICollection<MailAddress> cc = null,
            ICollection<MailAddress> bcc = null, string body = "", ICollection<Attachment> attachments = null)
        {
            MandateHasReceipient(to);
            MandateNotNull(from, nameof(from));

            try
            {
                cc = cc ?? new List<MailAddress>();
                bcc = bcc ?? new List<MailAddress>();
                attachments = attachments ?? new List<Attachment>();

                var message = new MailMessage();
                message.From = from;
                foreach (var item in to.Where(x => x != null))
                {
                    message.To.Add(new MailAddress(item.Address, item.DisplayName));
                }

                foreach (var item in cc.Where(x => x != null))
                {
                    message.CC.Add(new MailAddress(item.Address, item.DisplayName));
                }

                foreach (var item in bcc.Where(x => x != null))
                {
                    message.Bcc.Add(new MailAddress(item.Address, item.DisplayName));
                }

                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = !string.IsNullOrWhiteSpace(body);
                foreach (var attachment in attachments?.Where(x => x != null))
                {
                    message.Attachments.Add(attachment);
                }

                var emailDto = new EmailDto();
                emailDto.To.AddRange(to.Select(x => new NameEmailPair(x.DisplayName, x.Address)));
                emailDto.CC.AddRange(cc.Select(x => new NameEmailPair(x.DisplayName, x.Address)));
                emailDto.BCC.AddRange(bcc.Select(x => new NameEmailPair(x.DisplayName, x.Address)));
                emailDto.From = new NameEmailPair(from.DisplayName, from.Address);
                emailDto.Body = message.Body;
                emailDto.Date = DateTime.Now;
                emailDto.MailNodeId = nodeId;
                emailDto.Subject = subject;

                return message;
            }
            catch (Exception e)
            {
                var error = new DarwinEMailCreationException($"Error attempting to create MailMessage", e);
                _logger.Error<DarwinMailHelper>(error, error.Message);
                throw error;
            }
        }
        private string PopulateTemplateWithTags(string htmlTemplate, ICollection<EmailTagModel> tags)
        {
            foreach (var tag in tags?.Where(x => !string.IsNullOrWhiteSpace(x.Tag)))
            {
                htmlTemplate = htmlTemplate.Replace(tag.Tag, tag.Value);
            }
            return htmlTemplate;
        }

        private void MandateNotNull(object obj, string paramName)
        {
            if (obj is null)
            {
                throw new NullReferenceException($"{paramName} cannot be null");
            }
        }

        private void MandateHasReceipient(ICollection<MailAddress> recipients)
        {
            if (recipients is null || !recipients.Any() || !recipients.Where(x => x != null).Any())
            {
                throw new NullReferenceException($"At least one recipient must be provided.");
            }
        }
    }
}
