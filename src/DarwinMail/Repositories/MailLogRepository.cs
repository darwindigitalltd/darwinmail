using DarwinMail.Models;
using DarwinMail.Models.Entities;
using DarwinMail.Models.Schema;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core.Scoping;
using static DarwinMail.Constants.DarwinMailConstants;

namespace DarwinMail.Repositories
{
    internal class MailLogRepository : IMailLogRepository
    {
        private readonly IScopeProvider _scopeProvider;
        public MailLogRepository(IScopeProvider scopeProvider)
        {
            _scopeProvider = scopeProvider;
        }

        public ICollection<EmailDto> GetSentEmailsByNode(int nodeId, int pageSize, int page,
            string emailAddress, DateTime? from, DateTime? to, string status)
        {
            using (var scope = _scopeProvider.CreateScope())
            {
                var model = new List<EmailDto>();
                var offset = page == 1 ? 0 : (page - 1) * pageSize;

                var entities = scope.Database
                    .Fetch<EmailLogSchema>()
                    .Where(x => x.MailNodeId == nodeId)
                    .Where(x => string.IsNullOrEmpty(emailAddress) ? true : x.To.Contains(emailAddress))
                    .Where(x => !from.HasValue ? true : x.Date.Date >= from.Value.Date.Date)
                    .Where(x => !to.HasValue ? true : x.Date.Date <= to.Value.Date.Date)
                    .Where(x => string.IsNullOrEmpty(status) || status =="all" ? true : status == "sent" ? x.Sent : !x.Sent)
                    .AsQueryable()
                    .OrderByDescending(x => x.Date)
                    .Skip(offset)
                    .Take(pageSize);

                scope.Complete();

                if (!entities.Any())
                    return model;

                foreach (var entity in entities)
                {
                    var dto = new EmailDto
                    {
                        BCC = string.IsNullOrEmpty(entity.BCC) ? new List<NameEmailPair>() : JsonConvert.DeserializeObject<List<NameEmailPair>>(entity.BCC),
                        CC = string.IsNullOrEmpty(entity.CC) ? new List<NameEmailPair>() : JsonConvert.DeserializeObject<List<NameEmailPair>>(entity.CC),
                        Body = entity.Body,
                        Date = entity.Date,
                        From = string.IsNullOrEmpty(entity.From) ? null : JsonConvert.DeserializeObject<NameEmailPair>(entity.From),
                        Id = entity.Id,
                        MailNodeId = entity.MailNodeId,
                        ReplyTo = string.IsNullOrEmpty(entity.ReplyTo) ? new List<NameEmailPair>() : JsonConvert.DeserializeObject<List<NameEmailPair>>(entity.ReplyTo),
                        Sent = entity.Sent,
                        Subject = entity.Subject,
                        To = string.IsNullOrEmpty(entity.To) ? new List<NameEmailPair>() : JsonConvert.DeserializeObject<List<NameEmailPair>>(entity.To),
                        Exception = string.IsNullOrEmpty(entity.Exception) ? null : JsonConvert.DeserializeObject<Exception>(entity.Exception)
                    };
                    model.Add(dto);
                }
                return model;
            }
        }

        public (int, int) GetSentStatisticForMailNode(int nodeId)
        {
            using (var scope = _scopeProvider.CreateScope())
            {
                var sentCount = scope.Database
                    .ExecuteScalar<int>(
                    $"SELECT COUNT (Id) FROM {SchemaConstants.MailLogTableName} " +
                    $"WHERE MailNodeId = {nodeId} AND sent = 1");

                var failedCount = scope.Database
                   .ExecuteScalar<int>(
                   $"SELECT COUNT (Id) FROM {SchemaConstants.MailLogTableName} " +
                   $"WHERE MailNodeId = {nodeId} AND sent = 0");
                scope.Complete();
                return (sentCount, failedCount);
            }
        }

        public async Task<bool> Create(EmailDto email)
        {
            using (var scope = _scopeProvider.CreateScope())
            {
                var entity = new EmailLogSchema
                {
                    BCC = JsonConvert.SerializeObject(email.BCC),
                    CC = JsonConvert.SerializeObject(email.CC),
                    Body = email.Body,
                    Date = email.Date,
                    From = JsonConvert.SerializeObject(email.From),
                    MailNodeId = email.MailNodeId,
                    ReplyTo = JsonConvert.SerializeObject(email.ReplyTo),
                    Sent = email.Sent,
                    Subject = email.Subject,
                    To = JsonConvert.SerializeObject(email.To),
                    Exception = email.Exception != null ? JsonConvert.SerializeObject(email.Exception) : null
                };
                await scope.Database.InsertAsync<EmailLogSchema>(entity);
                scope.Complete();

                return true;
            }
        }

        public EmailDto GetEmailById(int emailId)
        {
            using (var scope = _scopeProvider.CreateScope())
            {
                var entity = scope.Database
                    .Fetch<EmailLogSchema>().Where(x => x.Id == emailId).FirstOrDefault();

                scope.Complete();

                if (entity is null)
                    return null;

                return new EmailDto
                {
                    BCC = string.IsNullOrEmpty(entity.BCC) ? new List<NameEmailPair>() : JsonConvert.DeserializeObject<List<NameEmailPair>>(entity.BCC),
                    CC = string.IsNullOrEmpty(entity.CC) ? new List<NameEmailPair>() : JsonConvert.DeserializeObject<List<NameEmailPair>>(entity.CC),
                    Body = entity.Body,
                    Date = entity.Date,
                    From = string.IsNullOrEmpty(entity.From) ? null : JsonConvert.DeserializeObject<NameEmailPair>(entity.From),
                    Id = entity.Id,
                    MailNodeId = entity.MailNodeId,
                    ReplyTo = string.IsNullOrEmpty(entity.ReplyTo) ? new List<NameEmailPair>() : JsonConvert.DeserializeObject<List<NameEmailPair>>(entity.ReplyTo),
                    Sent = entity.Sent,
                    Subject = entity.Subject,
                    To = string.IsNullOrEmpty(entity.To) ? new List<NameEmailPair>() : JsonConvert.DeserializeObject<List<NameEmailPair>>(entity.To),
                    Exception = string.IsNullOrEmpty(entity.Exception) ? null : JsonConvert.DeserializeObject<Exception>(entity.Exception)
                };
            }
        }
    }
}
