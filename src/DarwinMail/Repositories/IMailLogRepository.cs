using DarwinMail.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace DarwinMail.Repositories
{
    public interface IMailLogRepository
    {
        Task<bool> Create(EmailDto email);
        (int, int) GetSentStatisticForMailNode(int nodeId);
        ICollection<EmailDto> GetSentEmailsByNode(int nodeId, int pageSize, int page,
           string email, DateTime? from, DateTime? to, string status);
        EmailDto GetEmailById(int emailId);
    }
}
