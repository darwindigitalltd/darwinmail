﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DarwinMail.Constants
{
    public static class DarwinMailConstants
    {
        public static class Tags
        {
            public static readonly string TagOpen = "[#";
            public static readonly string TagClose = "#]";
        }

        public static class DocTypeAlias
        {
            public static readonly string MailNodeDocTypeAlias = "darwinMailNode";
        }
        public static class PropertyEditors
        {
            public static readonly string MultipleEmailAddressEditorAlias = "DarwinMail.MultipleEmailAddressEditor";
            public static readonly string EmailAddressEditorAlias = "DarwinMail.EmailAddressEditor";
            public static readonly string CustomTagsEditorAlias = "DarwinMail.CustomTagsEditor";
        }

        public static class SchemaConstants
        {
            public static readonly string MailLogTableName = "DarwinMailLog";
            public static readonly string MailStatisticsTableName = "DarwinMailStatistics";
        }
    }

}