﻿using Umbraco.Core.Migrations;
using Umbraco.Core.Logging;
using static DarwinMail.Constants.DarwinMailConstants;
using DarwinMail.Models.Schema;

namespace DarwinMail.Migrations
{
    public class CreateMailLogTableMigration : MigrationBase
    {
        public CreateMailLogTableMigration(IMigrationContext context) : base(context)
        {
        }

        public override void Migrate()
        {
            Logger.Info<CreateMailLogTableMigration>("Running migration {MigrationStep}", nameof(CreateMailLogTableMigration));

            if (this.TableExists(SchemaConstants.MailLogTableName))
            {
                Logger.Info<CreateMailLogTableMigration>($"{SchemaConstants.MailLogTableName} table exists, skipping creation.");
                return;
            }

            Logger.Info<CreateMailLogTableMigration>($"Creating {SchemaConstants.MailLogTableName} table.");
            Create.Table<EmailLogSchema>().Do();
            Logger.Info<CreateMailLogTableMigration>($"{SchemaConstants.MailLogTableName} table created.");
        }
    }
}