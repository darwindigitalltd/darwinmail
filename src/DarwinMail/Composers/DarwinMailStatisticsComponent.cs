﻿using DarwinMail.ContentApps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Composing;
using Umbraco.Web;

namespace DarwinMail.Composers
{
    public class DarwinMailStatisticsComponent : IUserComposer
    {
        public void Compose(Composition composition)
        {
            composition.ContentApps().Append<DarwinMailStatisticsContentApp>();
        }
    }
}