using DarwinMail.Repositories;
using DarwinMail.Services;

using Umbraco.Core;
using Umbraco.Core.Composing;

namespace DarwinMail.Composers
{
    internal class DarwinMailComposer : IUserComposer
    {
        public void Compose(Composition composition)
        {
            composition.Register<IDarwinMailHelper, DarwinMailHelper>();
            composition.Register<IMailLogRepository, MailLogRepository>();
        }
    }
}
