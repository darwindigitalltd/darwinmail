using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DarwinMail.Models.Entities
{
    public class EmailDto
    {
        public int Id { get; set; }
        public List<NameEmailPair> To { get; set; }
        public NameEmailPair From { get; set; }
        public List<NameEmailPair> ReplyTo { get; set; }
        public List<NameEmailPair> CC { get; set; }
        public List<NameEmailPair> BCC { get; set; }
        public DateTime Date { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public int MailNodeId { get; set; }
        public bool Sent { get; set; }
        public Exception Exception { get; set; }

        public EmailDto()
        {
            To = new List<NameEmailPair>();
            CC = new List<NameEmailPair>();
            BCC = new List<NameEmailPair>();
            ReplyTo = new List<NameEmailPair>();
        }
    }
}
