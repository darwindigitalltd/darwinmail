using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace DarwinMail.Models.Schema
{
    [TableName("DarwinMailLog")]
    [PrimaryKey("Id", AutoIncrement = true)]
    [ExplicitColumns]
    public class EmailLogSchema
    {
        [PrimaryKeyColumn(AutoIncrement = true, IdentitySeed = 1000)]
        [Column("Id")]
        public int Id { get; set; }

        [Column("to")]
        [Length(500)]
        public string To { get; set; }
        
        [Column("from")]
        [SpecialDbType(SpecialDbTypes.NTEXT)]
        public string From { get; set; }
        
        [Column("replyTo")]
        [Length(500)]
        [NullSetting(NullSetting = NullSettings.Null)]
        public string ReplyTo { get; set; }

        [Column("cc")]
        [SpecialDbType(SpecialDbTypes.NTEXT)]
        [NullSetting(NullSetting = NullSettings.Null)]
        public string CC { get; set; }

        [Column("BCC")]
        [NullSetting(NullSetting = NullSettings.Null)]
        [SpecialDbType(SpecialDbTypes.NTEXT)]
        public string BCC { get; set; }
        
        [Column("date")]
        public DateTime Date { get; set; }

        [Column("subject")]
        [NullSetting(NullSetting = NullSettings.Null)]
        [Length(500)]
        public string Subject { get; set; }

        [Column("body")]
        [SpecialDbType(SpecialDbTypes.NTEXT)]
        public string Body { get; set; }

        [Column("mailNodeId")]
        public int MailNodeId { get; set; }
        
        [Column("sent")]
        public bool Sent { get; set; }

        [Column("exception")]
        [NullSetting(NullSetting = NullSettings.Null)]
        [SpecialDbType(SpecialDbTypes.NTEXT)]
        public string Exception { get; set; }

    }
}
