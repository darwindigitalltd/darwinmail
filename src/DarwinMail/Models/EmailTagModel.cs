﻿using DarwinMail.Constants;
using DarwinMail.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DarwinMail.Models
{
    public class EmailTagModel
    {
        public string Tag { get; set; }
        public string Value { get; set; }

        public EmailTagModel() { }
        public EmailTagModel(string tag, string value)
        {
            if (!(tag.StartsWith(DarwinMailConstants.Tags.TagOpen) && tag.EndsWith(DarwinMailConstants.Tags.TagClose)))
                throw new InvalidTagException($"Mail tags must start wwith {DarwinMailConstants.Tags.TagOpen} and end end with {DarwinMailConstants.Tags.TagClose}");

            Tag = tag;
            Value = value;
        }
    }
}