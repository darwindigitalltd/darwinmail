using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DarwinMail.Models
{
    public class NameEmailPair
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public NameEmailPair(string name, string email)
        {
            Name = name;
            Email = email;
        } 
    }
}
