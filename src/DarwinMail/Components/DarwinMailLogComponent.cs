using DarwinMail.Migrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Composing;
using Umbraco.Core.Logging;
using Umbraco.Core.Migrations;
using Umbraco.Core.Migrations.Upgrade;
using Umbraco.Core.Scoping;
using Umbraco.Core.Services;
using static DarwinMail.Constants.DarwinMailConstants;

namespace DarwinMail.Components
{
    public class DarwinMailLogComponent : IComponent
    {
        private IScopeProvider _scopeProvider;
        private IMigrationBuilder _migrationBuilder;
        private IKeyValueService _keyValueService;
        private ILogger _logger;

        public DarwinMailLogComponent(
            IScopeProvider scopeProvider,
            IMigrationBuilder migrationBuilder,
            IKeyValueService keyValueService,
            ILogger logger)
        {
            _scopeProvider = scopeProvider;
            _migrationBuilder = migrationBuilder;
            _keyValueService = keyValueService;
            _logger = logger;
        }
        public void Initialize()
        {
            var mailLogMigrationPlan = new MigrationPlan(SchemaConstants.MailLogTableName);

            mailLogMigrationPlan.From(string.Empty).To<CreateMailLogTableMigration>("Create darwin Mail maillog table");
            mailLogMigrationPlan.From("Create darwin Mail maillog table").To<CreateMailLogTableMigration>("Create darwin Mail maillog table2");
            mailLogMigrationPlan.From("Create darwin Mail maillog table2").To<CreateMailLogTableMigration>("Create darwin Mail maillog table3");
            //mailLogMigrationPlan.From("Create darwin Mail maillog table")
            //    .ToWithReplace<CreateMailLogTableMigration, CreateMailLogTableMigration>("Create darwin Mail maillog table", "Create maillog");
            var upgrader = new Upgrader(mailLogMigrationPlan);
            upgrader.Execute(_scopeProvider, _migrationBuilder, _keyValueService, _logger);
        }

        public void Terminate()
        {
        }
    }
}
