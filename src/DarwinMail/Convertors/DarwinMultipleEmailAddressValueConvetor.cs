﻿using DarwinMail.Constants;
using DarwinMail.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.PropertyEditors;

namespace DarwinMail.Convertors
{
    public class DarwinMultipleEmailAddressValueConvetor : PropertyValueConverterBase, IPropertyValueConverter
    {
        public override object ConvertIntermediateToObject(IPublishedElement owner, IPublishedPropertyType propertyType, 
            PropertyCacheLevel referenceCacheLevel, object inter, bool preview)
        {
            if(inter != null)
            {
                var list = inter as List<NameEmailPair>;
                return list;
            }
            return new List<NameEmailPair>();
        }

        public override object ConvertIntermediateToXPath(IPublishedElement owner, IPublishedPropertyType propertyType,
            PropertyCacheLevel referenceCacheLevel, object inter, bool preview)
        {
            throw new NotImplementedException();
        }

        public override object ConvertSourceToIntermediate(IPublishedElement owner, IPublishedPropertyType propertyType, 
            object source, bool preview)
        {
            var list = JsonConvert.DeserializeObject<List<NameEmailPair>>(source.ToString())?.Where(x => !string.IsNullOrEmpty(x.Email)).ToList();


            return list;
        }

        public override PropertyCacheLevel GetPropertyCacheLevel(IPublishedPropertyType propertyType)
        {
            return PropertyCacheLevel.Elements;
        }

        public override Type GetPropertyValueType(IPublishedPropertyType propertyType)
        {
            return typeof(List<NameEmailPair>);
        }

        public override bool  IsConverter(IPublishedPropertyType propertyType)
        {
            return propertyType.EditorAlias.Equals(DarwinMailConstants.PropertyEditors.MultipleEmailAddressEditorAlias);
        }

        public override bool? IsValue(object value, PropertyValueLevel level)
        {
            return base.IsValue(value, level);
        }
    }    
}