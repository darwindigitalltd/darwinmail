﻿using DarwinMail.Constants;
using DarwinMail.Models;
using DarwinMail.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.PropertyEditors;

namespace DarwinMail.Convertors
{
    public class DarwinEmailAddressValueConvetor : PropertyValueConverterBase, IPropertyValueConverter
    {
        public override object ConvertIntermediateToObject(IPublishedElement owner, IPublishedPropertyType propertyType, 
            PropertyCacheLevel referenceCacheLevel, object inter, bool preview)
        {
            try
            {
                return inter as NameEmailPair;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public override object ConvertIntermediateToXPath(IPublishedElement owner, IPublishedPropertyType propertyType, 
            PropertyCacheLevel referenceCacheLevel, object inter, bool preview)
        {
            throw new NotImplementedException();
        }

        public override object ConvertSourceToIntermediate(IPublishedElement owner, IPublishedPropertyType propertyType,
            object source, bool preview)
        {
            try
            {
                var list = JsonConvert.DeserializeObject<NameEmailPair>(source.ToString());
                return list;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override PropertyCacheLevel GetPropertyCacheLevel(IPublishedPropertyType propertyType)
        {
            return PropertyCacheLevel.Elements;
        }

        public override Type GetPropertyValueType(IPublishedPropertyType propertyType)
        {
            return typeof(NameEmailPair);
        }

        public override bool  IsConverter(IPublishedPropertyType propertyType)
        {
            return propertyType.EditorAlias.Equals(DarwinMailConstants.PropertyEditors.EmailAddressEditorAlias);
        }

        public override bool? IsValue(object value, PropertyValueLevel level)
        {
            return base.IsValue(value, level);
        }
    }

    
}