angular.module('umbraco.resources').factory('mailStatisticsResource', function ($q, $http, umbRequestHelper) {
        return {
            getSentEmails: function (currentNodeId, pageNumber, email, from, to, status) {
                let url = "backoffice/DarwinMail/DarwinMailApi/GetSentEmails?nodeId=" + currentNodeId;
                if (pageNumber) {
                    url += "&page=" + pageNumber;
                }
                if (email) {
                    url += "&email=" + email;
                }

                if (from) {
                    url += "&from=" + from;
                }

                if (to) {
                    url += "&to=" + to;
                }

                if (status) {
                    url += "&status=" + status;
                }
                console.log(url);
                return umbRequestHelper.resourcePromise(
                    $http.get(url, "Failed to retrieve sent emails for node id " + currentNodeId));
            },

            getSentStatisticForMailNode: function(nodeId) {
                return umbRequestHelper.resourcePromise(
                    $http.get("backoffice/DarwinMail/DarwinMailApi/GetSentStatisticForMailNode?nodeId=" + nodeId),
                    "Failed to retrieve stats for mail node id" + nodeId);
            },

            resendEmail: function (emailId) {
                return umbRequestHelper.resourcePromise(
                    $http.post("backoffice/DarwinMail/DarwinMailApi/ResendEmail?emailId=" + emailId),
                    "Failed to resend email with Id" + emailId);
            },

            sendTestEmail: function (nodeId) {
                return umbRequestHelper.resourcePromise(
                    $http.post("backoffice/DarwinMail/DarwinMailApi/SendTestEmail?nodeId=" + nodeId),
                    "Failed to send test node email. make sure to and from have been completed," +
                    "and the node saved and published." + nodeId);
            } 
        };
    }
);
