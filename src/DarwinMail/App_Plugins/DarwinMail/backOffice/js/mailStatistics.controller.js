angular.module("umbraco").controller("DarwinMail.MailNodeStatisticsController",
    function ($scope, editorState, userService, contentResource, mailStatisticsResource) {
        let vm = this;

        console.log('vm', vm);
        vm.CurrentNodeId = editorState.current.id;
        vm.CurrentNodeAlias = editorState.current.contentTypeAlias;

        vm.columns = [
            "Recipient", "Date", "Subject", "Status", "Options"
        ];

        vm.PageNumber = 1;
        vm.FromDate = '';
        vm.ToDate = '';
        vm.Email = '';
        vm.Status = 'all';
        vm.Emails = [

        ];
        vm.EmailsSent = 0;
        vm.EmailsFailed = 0;
        vm.DisplayedEmail;

        vm.DisplayEmail = (index) => {
            vm.DisplayedEmail = vm.Emails[index];
            vm.DisplayedEmail.Body = vm.DisplayedEmail.Sent ? vm.DisplayedEmail.Body : vm.DisplayedEmail.Exception.Message;

            vm.CloseMailDiv.style.display = 'block';
            vm.ShowMailDivContainer.style.height = 500;
            vm.ShowMailDivContainer.style.width = 200;
            vm.ShowMailDivContainer.style.display = 'block';
        };

        vm.ResendEmail = (emailId) => {
            mailStatisticsResource.resendEmail(emailId).then(function (response) {
                loadEmails(vm);
            });
        };

        angular.element(document).ready(function () {
            if (vm.CurrentNodeId != 0) {
                loadEmails(vm);
                loadStats();
            }
        });

        vm.filter = function (e) {
            loadEmails(vm);
        };

        vm.ShowMailDivContainer = document.getElementById('emailOverlayContainer');
        vm.ShowMailDivContainer.style.display = 'none';

        vm.CloseMailDiv = document.getElementById('closeOverlay');
        if (vm.CloseMailDiv) {
            vm.CloseMailDiv.addEventListener('click', function () {
                vm.CloseMailDiv.parentElement.style.display = 'none';
            });
        }

        let loadEmails = (model) => {
            let fromNode = document.getElementById('fromDate');
            let toNode = document.getElementById('toDate');
            let from = fromNode ? fromNode.value : '';
            let to = toNode ? toNode.value : '';
            mailStatisticsResource.getSentEmails(vm.CurrentNodeId, vm.PageNumber, vm.Email, from, to, vm.Status)
                .then(function (response) {
                    vm.Emails = response.Data;
                });
        };

        let loadStats = () => mailStatisticsResource.getSentStatisticForMailNode(vm.CurrentNodeId).then(function (response) {
            console.log(response.Data);
            vm.EmailsSent = response.Data.Item1;
            vm.EmailsFailed = response.Data.Item2;
        });

    })
    .directive("strMailheader", function () {
        return {
            template: '<div class="statistics-email-viewer">' +
                '<div class="statistics-email-viewer__header">' +
                    '<div class="statistics-email-viewer__header__row">' +
                        '<label class="statistics-email-viewer__header__row__label">From:</label>' +
                        '<div class="statistics-email-viewer__header__row__item">{{ vm.DisplayedEmail.From.Email }} {{ vm.DisplayedEmail.From.Name }} </div>' +
                   ' </div>' +
                    '<div class="statistics-email-viewer__header__row">' +
                        '<label class="statistics-email-viewer__header__row__label">Date: </label>' +
                        '<div class="statistics-email-viewer__header__row__item">{{ vm.DisplayedEmail.Date }}</div>' +
                    '</div>' +
                    '<div class="statistics-email-viewer__header__row">' +
                        '<label class="statistics-email-viewer__header__row__label">Subject: </label>' +
                        '<div class="statistics-email-viewer__header__row__item">{{ vm.DisplayedEmail.Subject }}</div>' +
                    '</div>' +
                    '<div ng-show="vm.DisplayedEmail.To.length" class="statistics-email-viewer__header__row">' +
                        '<label class="statistics-email-viewer__header__row__label">To: </label>' +
                        '<span class="statistics-email-viewer__header__row__item" ng-repeat="email in vm.DisplayedEmail.To">' +
                          '  {{ email.Name }} <{{ email.Email }}>' +
                        '</span>' +
                    '</div>' +
                    '<div class="statistics-email-viewer__header__row">' +
                     '   <label class="statistics-email-viewer__header__row__label">CC: </label>' +
                     '   <div class="statistics-email-viewer__header__row__item" ng-repeat="email in vm.DisplayedEmail.CC">' +
                     '       {{ email.Name }} <{{ email.Email }}> ' +
                    '    </div>' +
                   ' </div>' +
                   ' <div class="statistics-email-viewer__header__row">' +
                   '     <label class="statistics-email-viewer__header__row__label">BCC: </label>' +
                  '      <div class="statistics-email-viewer__header__row__item" ng-repeat="email in vm.DisplayedEmail.BCC">' +
                  '          {{ email.Name }} <{{ email.Email }}>' +
                 '       </div>' +
                 '   </div>' +
              '  </div>' +
              '  <div ng-bind-html="vm.DisplayedEmail.Body" class="statistics-email-viewer__body">' +
              '  </div>' +
           ' </div>'
        }
    });

