angular.module("umbraco").controller("DarwinMail.EmailAddressEditor", function ($scope, assetsService, mailStatisticsResource, editorState, notificationsService ) {
    var initModelValue = $scope.$watch('$scope.model.value', function (model) {
        if (typeof $scope.model.value === 'string' && $scope.model.value.length == 0) {
            $scope.model.value = {
                Name: '',
                Email: ''
            };
        }
        initModelValue();
    });

    $scope.model.showTestButton = editorState.current.contentTypeAlias === 'darwinMailNode';

    let SendTestEmail = () => {
        mailStatisticsResource.sendTestEmail(editorState.current.id).then(function (response) {
            
            if (response) {
                notificationsService.success("Test email sent successfully.");
            }
            else {
                notificationsService.error("Failed to send test email. Make sure To and From have been provided, as well as checking logs.");
            }
        });
    };

    $scope.model.SendTest = SendTestEmail;
});

