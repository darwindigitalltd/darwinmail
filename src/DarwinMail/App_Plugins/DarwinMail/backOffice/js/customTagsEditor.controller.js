angular.module("umbraco").controller("DarwinMail.CustomTagsEditor", function ($scope, assetsService) {
    var initModelValue = $scope.$watch('$scope.model.value', function (model) {
        if (typeof $scope.model.value === 'string' && $scope.model.value.length == 0) {
            $scope.model.value = [{
                Tag: '',
                Value: ''
            }];
        }
        initModelValue();
    });

    $scope.AddToList = function AddNewEntryToTagList() {
        if ($scope.model.value == "") {
            $scope.model.value = [];
        }
        $scope.model.value.push({});
    };

    $scope.removeFromList = function (index) {
        if ($scope.model.value.length > 1) {
            $scope.model.value.splice(index, 1);
        }
        else {
            $scope.model.value[index].Tag = '';
            $scope.model.value[index].Value = '';
        } 
    };
    assetsService.loadCss("~/App_Plugins/DarwinMail/backoffice/css/darwinmail.min.css");
});

