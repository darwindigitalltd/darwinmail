angular.module("umbraco").controller("DarwinMail.MultipleEmailAddressEditor", function ($scope, assetsService) {

    var initModelValue = $scope.$watch('$scope.model.value', function (model) {
        if (typeof $scope.model.value === 'string' && $scope.model.value.length == 0) {

            $scope.model.value = [{
                Name: '',
                Email: ''
            }];
        }
        initModelValue();
    });

    $scope.AddToList = function AddNewEntryToEmailList() {
        if ($scope.model.value == "") {
            $scope.model.value = [];
        }

        $scope.model.value.push({});
    };

    $scope.removeFromList = function (index) {
        console.log('length', index);

        if ($scope.model.value.length > 1) {
            $scope.model.value.splice(index, 1);
        }
        else {
            $scope.model.value[index].Name = '';
            $scope.model.value[index].Email = '';
        }
    };

    assetsService.loadCss("~/App_Plugins/DarwinMail/backoffice/css/darwinmail.min.css");
});

