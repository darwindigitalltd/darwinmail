using System;

namespace DarwinMail.Exceptions
{
    public class DarwinMailNodeNotFoundException : Exception
    {
        public DarwinMailNodeNotFoundException(string message) : base(message) { }
        public DarwinMailNodeNotFoundException(string message, Exception inner) : base(message, inner) { }
    }

    public class DarwinEmailNotFoundException : Exception
    {
        public DarwinEmailNotFoundException(string message) : base(message) { }
        public DarwinEmailNotFoundException(string message, Exception inner) : base(message, inner) { }
    }

    public class InvalidTagException : Exception
    {
        public InvalidTagException(string message) : base(message) { }
        public InvalidTagException(string message, Exception inner) : base(message, inner) { }
    }

    public class DocTypeAliasException : Exception
    {
        public DocTypeAliasException(string message) : base(message) { }
        public DocTypeAliasException(string message, Exception inner) : base(message, inner) { }
    }

    public class DarwinEMailCreationException : Exception
    {
        public DarwinEMailCreationException(string message) { }
        public DarwinEMailCreationException(string message, Exception inner) : base(message, inner) { }
    }
}
