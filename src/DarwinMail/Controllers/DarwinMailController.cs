using DarwinMail.Exceptions;
using DarwinMail.Models;
using DarwinMail.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web;
using Umbraco.Web.Editors;
using Umbraco.Web.Mvc;

namespace DarwinMail.Controllers
{
    public class DarwinMailController : SurfaceController, IDarwinMailController
    {
        private readonly IDarwinMailHelper _mailHelper;
        public DarwinMailController(IDarwinMailHelper darwinMail)
        {
            _mailHelper = darwinMail;
        }

        public void SendTestEmail(int nodeId)
        {
            _mailHelper.SendTestMail(nodeId);
        }
    }
}
