﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.Editors;
using Umbraco.Web.Mvc;

namespace DarwinMail.Controllers.Api
{
    public abstract class ApiPluginBaseController : UmbracoAuthorizedJsonController
    {
        protected JsonNetResult _result;
        public ApiPluginBaseController()
        {
            _result = new JsonNetResult
            {
                SerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }
            };
        }
    }
}