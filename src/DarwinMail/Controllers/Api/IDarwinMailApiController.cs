using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.Mvc;

namespace DarwinMail.Controllers.Api
{
    public interface IDarwinMailApiController
    {
        JsonNetResult GetSentEmails(int nodeId, int page = 1, string email = "",
            DateTime? from = null, DateTime? to = null, string status = "");
        JsonNetResult GetSentStatisticForMailNode(int nodeId);
        bool ResendEmail(int emailId);
        bool SendTestEmail(int nodeId);
    }
}
