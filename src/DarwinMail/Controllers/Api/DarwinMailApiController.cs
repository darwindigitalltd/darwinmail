using DarwinMail.Models;
using DarwinMail.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Web.Mvc;

namespace DarwinMail.Controllers.Api
{
    [PluginController("DarwinMail")]
    public class DarwinMailApiController : ApiPluginBaseController, IDarwinMailApiController
    {
        private readonly IDarwinMailHelper _mailHelper;
        private const int PAGE_SIZE = 10;
        public DarwinMailApiController(IDarwinMailHelper darwinMail)
        {
            _mailHelper = darwinMail;
        }

        public JsonNetResult GetSentEmails(int nodeId, int page = 1, string email = "",
            DateTime? from = null, DateTime? to = null, string status = "")
        {
            var emails = _mailHelper.GetSentEmails(nodeId, PAGE_SIZE, page, email, from, to, status);
            _result.Data = emails;
            return _result;
        }

        public bool ResendEmail(int emailId)
        {
            return _mailHelper.ResendEmail(emailId);
        }
        public bool SendTestEmail(int nodeId)
        {
            return _mailHelper.SendTestMail(nodeId);
        }

        public JsonNetResult GetSentStatisticForMailNode(int nodeId)
        {
            _result.Data = _mailHelper.GetSentStatisticForMailNode(nodeId);
            return _result;
        }
    }
}
