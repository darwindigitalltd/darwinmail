using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.ContentEditing;
using Umbraco.Core.Models.Membership;

namespace DarwinMail.ContentApps
{
    public class DarwinMailStatisticsContentApp : IContentAppFactory
    {
        public ContentApp GetContentAppFor(object source, IEnumerable<IReadOnlyUserGroup> userGroups)
        {
            if (source is IContent)
            {
                try
                {
                    var content = source as Umbraco.Core.Models.Content;
                    if(content.ContentType.Alias == "darwinMailNode")
                    {
                        var wordCounterApp = new ContentApp
                        {
                            Alias = "DarwinMail.MailNodeStatistics",
                            Name = "Stats",
                            Icon = "icon-chart",
                            View = "/App_Plugins/DarwinMail/backOffice/editors/mailStatistics.html",
                            Weight = 1, // if less than -100, becomes default!
                        };
                        return wordCounterApp;
                    };
                }
                catch(Exception)
                {
                    return null;
                }
            }
            return null;
        }
    }
}
